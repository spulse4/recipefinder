package picture;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import javax.imageio.ImageIO;

public class PictureClipper {

	public static void main(String[] args){
		PictureClipper p = new PictureClipper();
//		p.find();

		String image = "win";
//
//		//Bigger top/bottom, but bring in left and right some
//		int total_left_right = 0;
//		int left = 0;
//		int total_top_bottom = 1840 + total_left_right;
//		int top = 900;
		
		//Bigger left/right, but bring in top and bottom some
		int total_top_bottom = 0;
		int top = 0;
		int total_left_right = 120 + total_top_bottom;
		int left = 60;

		p.clip(image, "top", top, true);
		p.clip("temp/" + image, "bottom", total_top_bottom - top, true);
		p.clip("temp/" + image, "left", left, true);
		p.clip("temp/" + image, "right", total_left_right - left, true);
		
		System.out.println("Done");		
	}
	
	public PictureClipper(){
		File folder = new File("pages/images/temp");
		if(!folder.exists()){
			try{
				Files.createDirectories(folder.toPath());
			}
			catch(IOException e){
				e.printStackTrace();
			}
		}
		File temp = new File("pages/images/temp");
		for(File inner : temp.listFiles()){
			try{
				Files.delete(inner.toPath());
			}
			catch(IOException e){
				e.printStackTrace();
			}
		}
	}
	
	public void clip(String name, String location, int pixels, boolean go){
		String loc = "pages/images";
		if(name.startsWith("temp/")){
			loc = loc + "/temp";
			name = name.substring(5);
		}
		File top = new File(loc);
		for(File inner : top.listFiles()){
			if(!inner.getName().toLowerCase().equals((name + ".png").toLowerCase())){
				continue;
			}
			try{
				BufferedImage input = ImageIO.read(inner);
				double width = input.getWidth();
				double height = input.getHeight();
				double minX = 0;
				double maxX = width;
				double minY = 0;
				double maxY = height;
				
				if(location.equals("top")){
					height = height - pixels;
					minY = pixels;
				}
				else if(location.equals("bottom")){
					height = height - pixels;
					maxY = height;
				}
				else if(location.equals("left")){
					width = width - pixels;
					minX = pixels;
				}
				else if(location.equals("right")){
					width = width - pixels;
					maxX = width;
				}
				
				System.out.println("New Width:  " + width);
				System.out.println("New Height: " + height);
				System.out.println("New Ratio:  " + width/height);
				if(go){
					BufferedImage output = new BufferedImage((int)width, (int)height, BufferedImage.TYPE_INT_RGB);
	
					for(int i = (int)minY; i < maxY; ++i){
						for(int j = (int)minX; j < maxX; ++j){
							output.setRGB(j - (int)minX, i - (int)minY, input.getRGB(j, i));
						}
					}
					
					File out = new File("pages/images/temp/" + inner.getName());
					ImageIO.write(output, "png", out);
				}
			}
			catch(IOException e){
				e.printStackTrace();
			}
		}
	}

	public void find(){
		File top = new File("pages/images");
		for(File inner : top.listFiles()){
			if(!inner.getName().endsWith(".png")){
				continue;
			}
			try{
				BufferedImage image = ImageIO.read(inner);
				double width = image.getWidth();
				double height = image.getHeight();
				double ratio = width/height;
				if(ratio != 1){
					System.out.println(inner.getName());
					System.out.println("  width:  " + width);
					System.out.println("  height: " + height);
					System.out.println("  " + ratio);
					if(ratio < 1){
						System.out.println("    Remove " + (height - width) + " from height");
					}
					else if(ratio > 1){
						System.out.println("    Remove " + (width - height) + " from width");
					}
				}
			}
			catch(IOException e){
				e.printStackTrace();
			}
		}
	}
}

package handlers;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.nio.file.Files;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import main.Util;

public class GeneralHandler implements HttpHandler{

	@Override
	public void handle(HttpExchange e) throws IOException{
		boolean index = false;
		String name = e.getRequestURI().toString();
		name = name.replace("%20", " ");
		if(name.equals("/")){
			name = Util.randomName() + ".html";
			Util.index(name);
			index = true;
		}
		File file = new File("pages" + name);
		if(!file.exists()){
			file = new File("pages/pages/404.html");
			e.getResponseHeaders().add("Content-Type", "text/html");
			e.sendResponseHeaders(HttpURLConnection.HTTP_NOT_FOUND, file.length());
			OutputStream outputStream = e.getResponseBody();
			Files.copy(file.toPath(), outputStream);
			e.getResponseBody().close();
			return;
		}
		e.getResponseHeaders().add("Content-Type", "text/html");
		e.sendResponseHeaders(HttpURLConnection.HTTP_OK, file.length());
		OutputStream outputStream = e.getResponseBody();
		Files.copy(file.toPath(), outputStream);
		if(index){
			try{
				Files.delete(file.toPath());
			}
			catch(IOException ex){
				ex.printStackTrace();
			}
		}
		e.getResponseBody().close();
	}
}

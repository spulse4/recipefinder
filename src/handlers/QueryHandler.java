package handlers;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import main.Util;

public class QueryHandler implements HttpHandler{

	@Override
	public void handle(HttpExchange e) throws IOException {	
		InputStream input = e.getRequestBody();
		int len = 0;
		
		byte[] buffer = new byte[1024];
		StringBuilder string = new StringBuilder();
		while(-1 != (len = input.read(buffer))){
			string.append(new String(buffer, 0, len));
		}
		
		List<String> ingredients = getIngredients(string.toString());
		String category = getCategory(string.toString());
		
		StringBuilder str = new StringBuilder();
		List<String> results = Util.filter(ingredients, category);
		for(int i = 0; i < results.size(); ++i){
			str.append(results.get(i));
		}
		e.sendResponseHeaders(HttpURLConnection.HTTP_OK, 0);
		OutputStreamWriter out = new OutputStreamWriter(e.getResponseBody());
		out.write(str.toString());
		out.flush();
		e.getResponseBody().close();
	}
	
	private List<String> getIngredients(String str){
		List<String> ingredients = new ArrayList<String>();
		if(str.startsWith("[]")){
			return new ArrayList<String>();
		}
		str = str.substring(1, str.indexOf(']'));
		Scanner scanner = new Scanner(str);
		scanner.useDelimiter(",");
		while(scanner.hasNext()){
			ingredients.add(scanner.next());
		}
		scanner.close();
		return ingredients;
	}
	private String getCategory(String str){
		return str.substring(str.lastIndexOf('[') + 1, str.lastIndexOf(']'));
	}
}

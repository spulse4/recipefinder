package handlers;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.nio.file.Files;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import main.Util;

public class BrowserHandler implements HttpHandler{

	@Override
	public void handle(HttpExchange e) throws IOException {
		String name = e.getRequestURI().toString().substring(7);
		name = name.replace("%20", " ");
		File file = new File("recipes" + name);
		if(!file.exists()){
			System.out.println(name);
			file = new File("pages/pages/404.html");
			e.getResponseHeaders().add("Content-Type", "text/html");
			e.sendResponseHeaders(HttpURLConnection.HTTP_NOT_FOUND, file.length());
			OutputStream outputStream = e.getResponseBody();
			Files.copy(file.toPath(), outputStream);
			e.getResponseBody().close();
			return;
		}
		String fileName = "temp" + Util.randomName() + ".html";
		Util.browse("recipes" + name, fileName);
		file = new File(fileName);
		e.getResponseHeaders().add("Content-Type", "text/html");
		e.sendResponseHeaders(HttpURLConnection.HTTP_OK, file.length());
		OutputStream outputStream = e.getResponseBody();
		Files.copy(file.toPath(), outputStream);
		try{
			Files.delete(file.toPath());
		}
		catch(IOException ex){
			ex.printStackTrace();
		}
		e.getResponseBody().close();
	}
}

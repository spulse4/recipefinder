package main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

import definitions.*;

public class Util {

	//TODO
	//Make sure that all the pictures have a recipe associated with it.
	
	public static void index(String filePath){
		StringBuilder string1 = new StringBuilder();
		StringBuilder string3 = new StringBuilder();
		StringBuilder string5 = new StringBuilder();
		
		string1.append("<html lang=\"en\">\n");
		string1.append("\t<head>\n");
		string1.append("\t\t<title>Recipe Finder</title>\n");
		string1.append("\t\t<meta charset=\"UTF-8\">\n");
		string1.append("\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/RecipeFinder.css\">\n");
		string1.append("\t\t<script src=\"/js/RecipeFinder.js\"></script>\n");
		string1.append("\t\t<script src=\"/js/ajax.js\"></script>\n");
		string1.append("\t</head>\n");
		string1.append("\t<body onload=\"init()\">\n");
		string1.append("\t\t<h1>\n");
		string1.append("\t\t\tRecipe Finder\n");
		string1.append("\t\t</h1>\n");
		string1.append("\t\t<a href=\"/browse/\" id=\"browse\">Browse Recipes</a>\n");
		string1.append("\t\t<p>\n");
		string1.append("\t\t\tList the ingredients that you have.<br>\n");
		string1.append("\t\t\tNarrow Ingredients<br>\n");
		string1.append("\t\t\t<input type=\"text\" id=\"filter\" onkeyup=\"filter()\" size=15>\n");
		string1.append("\t\t</p>\n");
		string1.append("\t\t<form id=\"form1\">\n");
		string1.append("\t\t\t<div id=\"sel1\">\n");
		string1.append("\t\t\t\t<select size=10 id=\"first\"></select>\n");
		string1.append("\t\t\t</div>\n");
		string1.append("\t\t\t<div id=\"buttons\">\n");
		string1.append("\t\t\t\t<div id=\"buttons1\">\n");
		string1.append("\t\t\t\t\t<button type=\"button\" id=\"add\" onclick=\"right()\">\n");
		string1.append("\t\t\t\t\t\t>>\n");
		string1.append("\t\t\t\t\t</button>\n");
		string1.append("\t\t\t\t\t<button type=\"button\" id=\"remove\" onclick=\"left()\">\n");
		string1.append("\t\t\t\t\t\t&lt;&lt;\n");
		string1.append("\t\t\t\t\t</button>\n");
		string1.append("\t\t\t\t</div>\n");
		string1.append("\t\t\t\t<div id=\"buttons2\">\n");
		string1.append("\t\t\t\t\t<button type=\"button\" id=\"addAll\" onclick=\"rightAll()\">\n");
		string1.append("\t\t\t\t\t\tAdd All\n");
		string1.append("\t\t\t\t\t</button>\n");
		string1.append("\t\t\t\t\t<button type=\"button\" id=\"removeAll\" onclick=\"leftAll()\">\n");
		string1.append("\t\t\t\t\t\tRemove All\n");
		string1.append("\t\t\t\t\t</button>\n");
		string1.append("\t\t\t\t</div>\n");
		string1.append("\t\t\t</div>\n");
		string1.append("\t\t\t<div id=\"sel2\">\n");
		string1.append("\t\t\t<select size=10 id=\"second\"></select>\n");
		string1.append("\t\t\t</div>\n");
		string1.append("\t\t\t<select size=10 id=\"third\">\n");

		string3.append("\t\t\t</select>\n");
		string3.append("\t\t</form>\n");
		string3.append("\t\t<form>\n");
		string3.append("\t\t\t<select size=10 id=\"type\" onchange=\"changedSelect()\">\n");

		string5.append("\t\t\t</select>\n");
		string5.append("\t\t</form>\n");
		string5.append("\t\t<input type=\"checkbox\" id=\"save\" checked>\n");
		string5.append("\t\tSave<br>\n");
		string5.append("\t\t<button type=\"button\" id=\"submit\" onclick=\"submitPage()\">\n");
		string5.append("\t\t\tSearch\n");
		string5.append("\t\t</button>\n");
		string5.append("\t\t<hr>\n");
		string5.append("\t\t<div id=\"demo\">\n");
		string5.append("\t\t\tNo Results\n");
		string5.append("\t\t</div>\n");
		string5.append("\t</body>\n");
		string5.append("</html>\n");

		try{
			StringBuilder str4 = new StringBuilder();
			Set<String> categories = new TreeSet<String>();
			File input = new File("recipes");

			categories.add("Any");
			//Gather all recipes
			for(File in : input.listFiles()){
				if(!in.isDirectory()){
					continue;
				}
				categories.add(in.getName());
			}
			
			//Add categories to String
			for(String str : categories){
				String ing = "\t\t\t\t<option value=\"" + str + "\">" + str + "</option>\n";
				str4.append(ing);
			}
			
			String st2 = Ingred.getIngredients();
			String st4 = str4.toString();

			FileWriter fw = new FileWriter("pages" + filePath);
			fw.write(string1.toString() + st2 + string3.toString() + st4 + string5.toString());
			fw.flush();
			fw.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public static List<String> filter(List<String> ingredients, String category){
		List<List<String>> matches = new ArrayList<List<String>>();
		List<String> results = new ArrayList<String>();
		
		//Gather all recipes
		File input = new File("recipes");
		List<Recipe> recipes = new ArrayList<Recipe>();
		for(File in : input.listFiles()){
			if(!in.isDirectory()){
				continue;
			}
			if(!(in.getName().equals(category) || category.equals("Any"))){
				continue;
			}
			for(File rec : in.listFiles()){
				if(!rec.getName().endsWith(".txt")){
					continue;
				}
				recipes.add(new Recipe(rec));
			}
		}
		
		for(Recipe r : recipes){
			int matchNumber = r.match(ingredients);
			while(matches.size() <= matchNumber){
				matches.add(new ArrayList<String>());
			}
			matches.get(matchNumber).add(r.getLink());
		}
		
		int count = 0;
		boolean first = true;
		for(int i = 0; i < matches.size(); ++i){
			List<String> match = matches.get(i);
			count += match.size();
			results.add("\t\t<p");
			if(first){
				first = false;
			}
			else{
				results.add(" id=\"par\"");
			}
			results.add(">\n");
			results.add("\t\t\t<div id=\"lin\">\n");
			results.add("\t\t\t\t<u>Recipes in which you need " + i + " more ingredients</u>\n");
			results.add("\t\t\t</div>\n");
			if(match.size() == 0){
				results.add("\t\t\tNo Recipes\n");
			}
			else{
				for(String str : match){
					results.add(str);
				}
			}
			results.add("\t\t</p>\n");
			if(count >= 50){
				break;
			}
		}		
		return results;
	}
	
	public static String randomName(){
		long time = System.currentTimeMillis();
		Random random = new Random();
		int r = random.nextInt();
		String encrypt = Long.toString(time) + r + random.toString();
		byte[] result = hash(encrypt.getBytes());
		return "/" + print(result);
	}
	
	public static void browse(String location, String tempFile){
		File top = new File(location);
		if(location.equals("recipes") || location.equals("recipes/")){
			location = "recipes/Home";
		}
		StringBuilder str = new StringBuilder();
		str.append("<html lang=\"en\">\n");
		str.append("\t<head>\n");
		str.append("\t\t<title>Recipe Finder</title>\n");
		str.append("\t\t<meta charset=\"UTF-8\">\n");
		str.append("\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/Browse.css\">\n");
		str.append("\t</head>\n");
		str.append("\t<body>\n");
		str.append("\t\t<h2>\n");
		str.append("\t\t\t" + location.substring(location.lastIndexOf("/") + 1) + "\n");
		str.append("\t\t</h2>\n");
		
		for(File inner : top.listFiles()){
			str.append("\t\t");
			str.append("<a href=\"");
			if(inner.getName().endsWith(".txt")){
				String temp = inner.getName();
				temp = temp.substring(0, temp.length() - 4);
				str.append("/get/" + location.substring(8) + "/" + temp);
				str.append("\">" + temp);
			}
			else{
				str.append(inner.getName() + "\">" + inner.getName());
			}
			str.append("</a><br>\n");
		}
		str.append("\t\t<br>\n");
		str.append("\t\t<a href=\"");
		if(top.getName().equals("recipes")){
			str.append("/");
		}
		else{
			str.append("/browse/");
		}
		str.append("\">Up a level</a><br>\n");
		
		str.append("\t</body>\n");
		str.append("</html>");
		
		try{
			FileWriter fw = new FileWriter(tempFile);
			fw.write(str.toString());
			fw.flush();
			fw.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	private static byte[] hash(byte[] in){
		try{
			byte[] out;
			java.security.MessageDigest d = null;
			d = java.security.MessageDigest.getInstance("SHA-1");
			d.reset();
			d.update(in);
			out = d.digest();
			return out;
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	private static String print(byte[] in){
		StringBuilder s = new StringBuilder();
		for(byte b : in){
			s.append(binToHex(unsign(b)));
		}
		return s.toString();
	}
	
	private static String unsign(byte b){
		int x = b;
		if(x < 0){
			x = x * -1;
			String str = Integer.toString(x, 2);
			while(str.length() != 8){
				str = "0" + str;
			}
			str = inverse(str);
			str = addOne(str);
			return str;
		}
		else{
			String str = Integer.toString(x, 2);
			while(str.length() != 8){
				str = "0" + str;
			}
			return str;
		}
	}
	private static String addOne(String a){
		StringBuilder str = new StringBuilder();
		boolean carryOver = true;
		for(int i = a.length() - 1; i > -1; --i){
			char temp = a.charAt(i);
			if(carryOver){
				if(temp == '1'){
					str.insert(0, '0');
				}
				else{
					str.insert(0, '1');
					carryOver = false;
				}
			}
			else{
				str.insert(0, temp);
			}
		}
		return str.toString();
	}
	private static String inverse(String a){
		StringBuilder str = new StringBuilder();
		for(int i = 0; i < a.length(); ++i){
			if(a.charAt(i) == '0'){
				str.append("1");
			}
			else{
				str.append("0");
			}
		}
		return str.toString();
	}
	private static String binToHex(String str){
		StringBuilder string = new StringBuilder();
		for(int i = 0; i < str.length(); i += 4){
			String temp = str.substring(i, i + 4);
			string.append(Integer.toString(Integer.valueOf(temp, 2), 16));
		}
		return string.toString();
	}
}

package main;

import java.io.IOException;
import java.net.InetSocketAddress;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import handlers.*;

public class RecipeFinder{
	
	private int SERVER_PORT_NUMBER;
	
	private HttpServer server;
	String persistenceType;
	
	private HttpHandler browser = new BrowserHandler();
	private HttpHandler general = new GeneralHandler();
	private HttpHandler query = new QueryHandler();
	private HttpHandler recipe = new RecipeHandler();
	
	public static void main(String[] args) {
		RecipeFinder server = new RecipeFinder();
		if (args.length == 1) {
			server.initPortNum(Integer.parseInt(args[0]));
		}
		else {
			server.initPortNum(80);
		}
		server.run();
		//new TestRecipes();
		System.out.println("Done");
	}

	public RecipeFinder() {}
	
	public void run() {
		try {
			server = HttpServer.create(new InetSocketAddress(SERVER_PORT_NUMBER), 10);
		} 
		catch (IOException e) {
			e.printStackTrace();
			return;
		}

		server.setExecutor(java.util.concurrent.Executors.newCachedThreadPool());
		
		init();
		
		server.start();
	}
			
	private void initPortNum(int port) {
		SERVER_PORT_NUMBER = port;
	}
	
	private void init(){
		server.createContext("/browse", browser);
		server.createContext("/", general);
		server.createContext("/query", query);
		server.createContext("/get/", recipe);
	}
}

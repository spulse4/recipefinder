package definitions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Substitutions {

	private static Map<Ingred, List<Ingred>> subs;
	
	static{
		subs = new HashMap<Ingred, List<Ingred>>();
		List<Ingred> substitutes = new ArrayList<Ingred>();
		
		//margarine
		substitutes.add(Ingred.butter);
		subs.put(Ingred.margarine, substitutes);
		substitutes = new ArrayList<Ingred>();
		
		//Oil
		substitutes.add(Ingred.oil_1canola2);
		substitutes.add(Ingred.oil_1olive2);
		substitutes.add(Ingred.oil_1vegetable2);
		subs.put(Ingred.oil, substitutes);
		substitutes = new ArrayList<Ingred>();

		//Canola Oil
		substitutes.add(Ingred.oil);
		substitutes.add(Ingred.oil_1olive2);
		substitutes.add(Ingred.oil_1vegetable2);
		subs.put(Ingred.oil_1canola2, substitutes);
		substitutes = new ArrayList<Ingred>();

		//Vegetable Oil
		substitutes.add(Ingred.oil);
		substitutes.add(Ingred.oil_1canola2);
		substitutes.add(Ingred.oil_1olive2);
		subs.put(Ingred.oil_1vegetable2, substitutes);
		substitutes = new ArrayList<Ingred>();
	}
	
	public static List<Ingred> getSubs(Ingred ing){
		if(subs.containsKey(ing)){
			return subs.get(ing);
		}
		return new ArrayList<Ingred>();
	}
}

package definitions;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Ingredient {
		
	private String amount;
	private boolean optional1;
	private boolean optional2;
	private List<String> ingredient;
	
	public Ingredient(String line) throws Exception{
		ingredient = new ArrayList<String>();
		Scanner scanner = new Scanner(line);
		scanner.useDelimiter("::");
		String temp = scanner.next();
		if(temp.equals("(Optional)")){
			optional1 = true;
			temp = scanner.next();
		}
		else{
			optional1 = false;
		}
		if(temp.startsWith("(Optional)")){
			System.out.println("Optional is not isolated");
			scanner.close();
			throw new Exception();
		}
		if(optional1 == false){
			Ingred.fromString(temp);
		}
		ingredient.add(temp);
		
		if(scanner.hasNext()){
			temp = scanner.next();
		}
		else{
			temp = "";
		}
		while(temp.equals("(Or)")){
			temp = scanner.next();
			Ingred.fromString(temp);
			ingredient.add(temp);
			if(scanner.hasNext()){
				temp = scanner.next();
			}
			else{
				temp = "";
			}
		}
		
		if(temp.equals("(Optional)")){
			optional2 = true;
			if(scanner.hasNext()){
				temp = scanner.next();
			}
			else{
				temp = "";
			}
		}
		else{
			optional2 = false;
		}
		amount = temp;
		if(amount.endsWith(".")){
			System.out.println("Ends with \".\"");
			scanner.close();
			throw new Exception();
		}
		
		scanner.close();
	}
	
	public List<String> getIngredientForList(){
		if(optional1){
			return new ArrayList<String>();
		}
		return ingredient;
	}
	public List<String> getIngredientForRecipe(){
		return ingredient;
	}
	
	public String getAmount(){
		if(optional1 || optional2){
			if(amount.equals("")){
				return "(Optional)";
			}
			return "(Optional) " + amount;
		}
		return amount;
	}
}

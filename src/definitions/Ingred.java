package definitions;

public enum Ingred {

	//A
	apples,
	//B
	bacon, baguette, baking_powder, baking_soda, bananas, basil, bay_leaves, beef,
	beef_1flank_steak2, beef_1ground2, bell_pepper_1green2, bell_pepper_1red2,
	bell_pepper_1yellow2, black_bean_garlic_sauce, bok_choy_1baby2, bread_crumbs, bread_cubes,
	broccoli_florets, brownie_mix, buffalo_sauce, butter, buttermilk,	
	//C
	cake_mix_1yellow2, carrot, celery, cheese_1asiago2, cheese_1cheddar2, cheese_1cottage2,
	cheese_1cream2, cheese_1mozzarella2, cheese_1parmesan2, cheese_1romano2, chicken,
	chicken_breasts, chicken_broth, chicken_stock, chicken_tenderloins, chicken_tenders,
	chicken_thighs, chives, chocolate_bars_with_almonds, chocolate_chips, cilantro,
	cinnamon_1ground2, cocoa, cool_whip, cornstarch, corn_flakes, cream_1mashmallow2,
	cream_of_mushroom_soup, cream_1sour2, cream_1whipping2, croutons, crushed_chili_pepper,
	cucumber, cucumber_1english2,
	//D
	daikon, dressing_1ranch2,
	//E
	egg, extract_1butter2, extract_1rum2, extract_1vanilla2,
	//F
	fish_sauce, flour, flour_1tapioca2, frozen_orange_juice_concentrate,
	//G
	garlic, garlic_1granulated2, garlic_powder, garnish, gelatin_1unflavored2, ginger,
	ginger_1ground2, ginger_root, graham_crackers, graham_cracker_crust,
	//H
	half_and_half, ham, heath_bars, honey,
	//I
	ice, italian_dressing_packet_mix,
	//J
	jalapenos,
	//K
	ketchup,
	//L
	lemon, lemon_curd, lemon_juice, lemon_rind, lemon_zest, lettuce_1romaine2, lime_juice,
	lumpia_wrappers,
	//M
	margarine, marshmallows, mayonnaise, milk, milk_1condensed2, mirin, mushrooms,
	//N
	nutmeg, nuts,
	//O
	oil, oil_1canola2, oil_1olive2, oil_1sesame2, oil_1vegetable2, olives, onion, onion_1dried2,
	onion_1green2, onion_1red2, onion_1sweet2, onion_1white2, onion_1yellow2, orange_juice,
	orange_zest, oregano,
	//P
	paprika, parsley, pasta, pasta_1fettuccine2, pasta_1spaghetti2, peaches, pecans, pepper,
	peppers, pineapple, pineapple_tidbits, popcorn, pork, pork_1ground2, potato,
	//Q
	//R
	raisins, rice, rosemary,
	//S
	salt, salt_1garlic2, salt_1rock2, scallions, shortening, shrimp, skor, soda_1cream2,
	soda_1dr3_pepper2, soy_sauce, spinach_1baby2, sriracha, strawberry, sugar, sugar_1brown2,
	sugar_1confectioners2, sugar_1powdered2, syrup_1chocolate2, syrup_1corn2, syrup_1maple2,
	//T
	tater_tots, teriyaki_sauce, thyme, tomato, tomato_1cherry2, tomato_1grape2, tortillas_1flour2,
	//U
	//V
	vinegar, vinegar_1apple2, vinegar_1red2, vinegar_1rice2, vinegar_1white2,
	//W
	walnuts, water, wine_1white2,
	//X
	//Y
	yeast,
	//Z
	zucchini;
	
	private static String listOfIngredients = null;
	
	public static Ingred fromString(String str) throws Exception{
		str = str.toLowerCase();
		str = str.replaceAll("[(]", "1");
		str = str.replaceAll("[)]", "2");
		str = str.replaceAll("[.]", "3");
//		str = str.replaceAll("[().]", "");
		str = str.replaceAll(" ", "_");
		for(Ingred in : Ingred.values()){
			if(in.toString().toLowerCase().equals(str)){
				return in;
			}
		}
		System.out.println(str + " is not defined");
		throw new Exception();
	}
	
	public static String getIngredients(){
		if(listOfIngredients == null){
			fillIngredients();
		}
		return listOfIngredients;
	}
	
	private static void fillIngredients(){
		StringBuilder total = new StringBuilder();
		for(Ingred ing : Ingred.values()){
			String str = ing.toString();
			str = str.replaceAll("1", "(");
			str = str.replaceAll("2", ")");
			str = str.replaceAll("3", ".");
			str = str.replace("_", " ");
			StringBuilder string = new StringBuilder();
			boolean space = true;
			for(int i = 0; i < str.length(); ++i){
				if(space && str.charAt(i) != '('){
					string.append(Character.toUpperCase(str.charAt(i)));
					space = false;
				}
				else{
					string.append(str.charAt(i));
				}
				if(str.charAt(i) == ' '){
					space = true;
				}
			}
			str = string.toString();
			total.append("\t\t\t\t<option value=\"" + str + "\">" + str + "</option>\n");
		}
		listOfIngredients = total.toString();
	}
}

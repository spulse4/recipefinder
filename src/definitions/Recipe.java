package definitions;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Recipe {

	private boolean main;
	private String name;
	private String url;
	private List<String> instructions;
	private List<Ingredient> ingredients;
	private List<Recipe> required;
	
	public Recipe(File input){
		instructions = new ArrayList<String>();
		ingredients = new ArrayList<Ingredient>();
		required = new ArrayList<Recipe>();
		if(!input.exists()){
			System.out.println("File:" + input.getAbsolutePath() + " does not exist");
			url = "";
			return;
		}
		String parent = input.getParentFile().getName();
		name = input.getName().substring(0, input.getName().indexOf('.'));
		url = "get/" + parent + "/" + name;
		try{
			StringBuilder str = new StringBuilder();
			Scanner scanner = new Scanner(input);
			while(scanner.hasNextLine()){
				str.append(scanner.nextLine());
				str.append("\n");
			}
			scanner.close();
			try{
				parseFile(str.toString());
			}
			catch(Exception e){
				System.out.println(url);
			}
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
	
	public String getName(){
		return name;
	}
	
	private void parseFile(String file) throws Exception{
		Scanner scanner = new Scanner(file);
		String temp = scanner.nextLine();
		if(temp.equals("Extra")){
			this.main = false;
		}
		else if(temp.equals("Main")){
			this.main = true;
		}
		else{
			System.out.println("Invalid Format, Missing File Type");
			System.out.print("    in File: ");
			scanner.close();
			throw new Exception();
		}
		temp = scanner.nextLine();
		if(!temp.equals("")){
			System.out.println("Invalid Format, Missing Space after file type");
			System.out.print("    in File: ");
			scanner.close();
			throw new Exception();
		}
		temp = scanner.nextLine();
		if(!temp.equals("Ingredients")){
			System.out.println("Invalid Format, Ingredients is missing");
			System.out.print("    in File: ");
			scanner.close();
			throw new Exception();
		}
		while(scanner.hasNextLine()){
			String line = scanner.nextLine();
			if(line.equals("")){
				break;
			}
			Scanner lineScanner = new Scanner(line);
			lineScanner.useDelimiter("::");
			String ingredient = lineScanner.next();
			if(ingredient.equals("(Recipe)")){
				String nextRecipe = lineScanner.next();
				lineScanner.close();
				Recipe rec = new Recipe(new File(nextRecipe));
				required.add(rec);
				continue;
			}
			lineScanner.close();
			try{
				Ingredient in = new Ingredient(line);
				ingredients.add(in);
			}
			catch(Exception e){
				System.out.println("Failed to load ingredients");
				System.out.println("  line: " + line);
				System.out.print("    in File: ");
				scanner.close();
				throw new Exception();
			}
		}
		temp = scanner.nextLine();
		if(temp.equals("Diretions")){
			System.out.println("Invalid Format, Missing Directions");
			System.out.print("    in File: ");
			scanner.close();
			throw new Exception();
		}
		while(scanner.hasNextLine()){
			temp = scanner.nextLine();
			if(temp.equals("Directions") || temp.equals("")){
				continue;
			}
			if(temp.equals("Instructions")){
				System.out.println("Instructions");
				scanner.close();
				throw new Exception();
			}
			if(!(temp.endsWith(".") || temp.endsWith(":"))){
				System.out.println("Direction doesn't end with a period");
				System.out.print("  in File: ");
				scanner.close();
				throw new Exception();
			}
			instructions.add(temp);
		}
		scanner.close();
	}
	
	public int match(List<String> ingredient){
		int match = 0;
		List<Ingredient> ings = new ArrayList<Ingredient>();
		for(Ingredient i : ingredients){
			ings.add(i);
		}
		for(Recipe rec : required){
			ings.addAll(rec.getIngredients());
		}
		for(Ingredient i : ings){
			List<String> ing = i.getIngredientForList();
			if(ing.size() == 0){
				continue;
			}
			boolean found = false;
			for(String str : ing){
				if(ingredient.contains(str)){
					found = true;
					break;
				}
				try{
					for(Ingred ingred : Substitutions.getSubs(Ingred.fromString(str))){
						for(String s : ingredient){
							if(Ingred.fromString(s) == ingred){
								found = true;
								break;
							}
						}
						if(found){
							break;
						}
					}
					if(found){
						break;
					}
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			if(found == false){
				++match;
				if(match >= 8){
					return 8;
				}
			}
		}
		return match;
	}
	
	public List<Ingredient> getIngredients(){
		return ingredients;
	}
	
	public List<String> getInstructions(){
		return instructions;
	}
	
	public String getURL(){
		return url;
	}
	
	public String getLink(){
		StringBuilder temp = new StringBuilder();
		temp.append("\t\t\t<div id=\"lin\">\n");
		temp.append("\t\t\t\t<img src=\"/images/" + name + ".jpg\" alt=\"");
		if(main){
			temp.append("N/A");
		}
		else{
			temp.append("Extra");
		}
		temp.append("\" />\n");
		temp.append("\t\t\t\t<a href=\"" + url + "\">" + name + "</a><br>\n");
		temp.append("\t\t\t</div>\n");
		return temp.toString();
	}
	
	public void getPage(String fileName){
		StringBuilder str = new StringBuilder();
		str.append("<DOCTYPE html>\n");
		str.append("<html>\n");
		str.append("\t<head>\n");
		str.append("\t\t<title>\n");
		str.append("\t\t\t" + name + "\n");
		str.append("\t\t</title>\n");
		str.append("\t\t<meta charset=\"UTF-8\">\n");
		str.append("\t\t<link rel=\"stylesheet\" type=\"text/css\" href=\"/css/recipe.css\">\n");
		str.append("\t</head>\n");
		str.append("\t<body>\n");
		str.append("\t\t<h2>\n");
		str.append("\t\t\t" + name + "\n");
		str.append("\t\t</h2>\n");
		if(main){
			str.append("\t\t<img src=\"/images/" + name + ".jpg\" alt=\"Haven't tried this yet\">\n");
		}
		str.append("\t\t<div");
		if(main){
			str.append(" id=\"ingredients\"");
		}
		str.append(">\n");
		str.append("\t\t\t<p>\n");
		str.append("\t\t\t\t<b>\n");
		str.append("\t\t\t\t\tIngredients:<br>\n");
		str.append("\t\t\t\t</b>\n");
		for(Ingredient ing : ingredients){
			List<String> i = ing.getIngredientForRecipe();
			String amount = ing.getAmount();
			str.append("\t\t\t\t" + amount + " ");
			if(i.size() == 1){
				str.append(i.get(0) + "<br>\n");
			}
			else{
				for(String string : i){
					str.append(string + " or ");
				}
				str.setLength(str.length() - 4);
				str.append("<br>\n");
			}
		}
		for(int i = 0; i < required.size(); ++i){
			Recipe rec = required.get(i);
			String name = rec.getName();
			str.append("\t\t\t\t<a href=\"/" + rec.getURL() + "\">" + name + "</a><br>\n");
			List<Ingredient> ingreds = rec.getIngredients();
			str.append("\t\t\t</p>\n");
			str.append("\t\t\t<p id=\"inner\">\n");
			for(Ingredient ing : ingreds){
				List<String> in = ing.getIngredientForRecipe();
				String amount = ing.getAmount();
				str.append("\t\t\t\t" + amount + " ");
				if(in.size() == 1){
					str.append(in.get(0) + "<br>\n");
				}
				else{
					for(String string : in){
						str.append(string + " or");
					}
					str.setLength(str.length() - 3);
					str.append("<br>\n");
				}
			}
			str.setLength(str.length() - 5);
//			str.append("\n");
			str.append("\t\t\t</p>\n");
		}
		str.setLength(str.length() - 5);
		str.append("\n");
		str.append("\t\t\t</p>\n");
		str.append("\t\t</div>\n");
		str.append("\t\t<br>\n");
		str.append("\t\t<div class=\"directions\">\n");
		str.append("\t\t\t<p>\n");
		str.append("\t\t\t\t<b>\n");
		str.append("\t\t\t\t\tDirections:<br>\n");
		str.append("\t\t\t\t</b>\n");
		for(String string : instructions){
			str.append("\t\t\t\t" + string + "<br>\n");
		}
		str.append("\t\t\t</p>\n");
		str.append("\t\t</div>\n");
		str.append("\t\t<br>\n");
//		str.append("\t\t<a href=\"" + name + ".docx\">\n");
//		str.append("\t\t\tDownload Document<br>\n");
//		str.append("\t\t</a>\n");
		str.append("\t\t<a href=\"/\">\n");
		str.append("\t\t\tHome\n");
		str.append("\t\t</a>\n");
		str.append("\t</body>\n");
		str.append("</html>");
		try{
			FileWriter fw = new FileWriter(new File(fileName));
			fw.write(str.toString());
			fw.flush();
			fw.close();
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
}

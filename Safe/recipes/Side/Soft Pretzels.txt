Main

Ingredients
Yeast::1 Pkg
Sugar::1 Tbsp
Water::1/4 C (Warm)
Flour::2 1/2 C
Salt::1/4 tsp
Milk::10 Tbsp
Baking Soda::2 Tbsp
Water::3 C (Hot)

Directions
In a small bowl, dissolve Yeast and Sugar in 1/4 C water.
In a large bowl, combine Flour and Salt.
Add yeast mixture and milk to flour mixture.
Turn on floured board and knead for 10 minutes.
Place in greased bowl, turning to grease top of dough.
Cover with towel and let rise about 45 minutes - 1 hour.
Preheat oven to 400 degrees F.
Mix Baking Soda in 3 C Water, bring to slow boil.
Cut dough into 8 equal pieces.
Roll each piece into a rope 18-20 inches long.
Form into pretzels and place on baking sheet.
Cover and let rise 15 minutes.
With broad spatula, lower pretzels into water.
Boil 10 seconds and turn over.
Boil 10 more seconds.
Place on greased baking sheet.
Sprinkle with Salt and Poppy Seeds.
Bake 10-12 minutes (Till brown).
Main

Ingredients
Pasta::3/4 Box Spiral or Fettechini
Chicken Breasts::3-4
Butter::1 Cube
Onion::1/4
Garlic::(Optional)::2 Cloves (Minced)
Half and Half::(Or)::Cream (Whipping)::1 Pint
Cheese (Parmesan)::1 C (Grated)
Cornstarch
Zucchini::(Optional)
Mushrooms::(Optional)

Directions
Cut Chicken Breasts up into strips and chunks.
Fry them in Butter Onion, and Garlic (if desired) on medium or low heat.
Add Half and Half or Whipping Cream and Parmesan Cheese.
Note: Do not let it boil or it will curdle.
If needed, thicken by adding a cornstarch slurry (2 Tbsp Cornstarch and 2-4 Tbsp Cold Water) in small amounts until desired thickness.
function init(){
	setup();
	checkSelected();
}
function setup(){
	leftAll();
	var cookie = document.cookie;
	if(cookie.indexOf("ingredients=") != -1){
		var cook = cookie.substring(cookie.indexOf("ingredients") + 12);
		if(cook.indexOf(";") != -1){
			cook = cook.substring(0, cook.indexOf(";"));
		}
		if(cook == ""){
			return;
		}
		else if(cook == "[all]"){
			rightAll();
			return;
		}
		while(cook.indexOf(",") != -1){
			var temp = cook.substring(0, cook.indexOf(","));
			selectValue(temp);
			right();
			cook = cook.substring(cook.indexOf(",") + 1);
		}
		selectValue(cook);
		right();
	}
}
function selectValue(str){
	var firstOptions = document.getElementById("first").options;
	for(i = 0; i < firstOptions.length; ++i){
		if(firstOptions[i].text.indexOf(str) != -1){
			document.getElementById("first").selectedIndex = i;
			return;
		}
	}
}
function changedSelect(){
	var checked = document.getElementById("save").checked;
	document.getElementById("save").checked = false;
	submitPage();
	document.getElementById("save").checked = checked;
	
}
function submitPage(){
	var checked = document.getElementById("save").checked;
	var str = "[";
	var second = document.getElementById("second").options;
	var type = document.getElementById("type").value;
	var i;
	for(i = 0; i < second.length; ++i){
		str = str + second[i].text + ",";
	}
	if(str.length > 1){
		str = str.substring(0, str.length - 1);
	}
	var cookie = str.substring(1);
	if(document.getElementById("first").options.length == 0){
		cookie = "[all]";
	}
	str = str + "][" + type + "]";
	
	var xhttp = new XMLHttpRequest();
	xhttp.open("POST", "/query", false);
	xhttp.send(str);
	document.getElementById("demo").innerHTML = xhttp.responseText;

	if(checked && cookie != ""){
		var CookieDate = new Date;
		CookieDate.setFullYear(CookieDate.getFullYear() + 10);
		document.cookie = "ingredients=" + cookie + "; expires=" + CookieDate.toGMTString() + ";path=/";
	}
	else if(checked && cookie == ""){
		document.cookie = "ingredients=; expires=Thu, 01 Jan 1970 00:00:00 UTC";
	}
}
function filter(){
	$("#first").empty();
	var filter = document.getElementById("filter").value.toLowerCase();
	var first = document.getElementById("first");
	var secondOptions = document.getElementById("second").options;
	var thirdOptions = document.getElementById("third").options;
	var i;
	var j;
	for(i = 0; i < thirdOptions.length; ++i){
		var found = 0;
		for(j = 0; j < secondOptions.length; ++j){
			if(thirdOptions[i].text == secondOptions[j].text){
				found = 1;
				break;
			}
		}
		if(found == 1){
			continue;
		}
		if(thirdOptions[i].text.toLowerCase().indexOf(filter) != -1){
			var option = document.createElement("option");
			option.text = thirdOptions[i].text;
			first.add(option);
		}
	}
	
	sortAll();
}
function rightAll(){
	$("#first").empty();
	$("#second").empty();
	var second = document.getElementById("second");
	var third = document.getElementById("third").options;
	var i;
	for(i = 0; i < third.length; ++i){
		var option = document.createElement("option");
		option.text = third[i].text;
		second.add(option);
	}
	filter();
	checkSelected();
}
function right(){
	var first = document.getElementById("first");
	var second = document.getElementById("second");
	var selectedFirst = first.selectedIndex;
	var selectedSecond = second.selectedIndex;
	if(selectedFirst == -1){
		return;
	}
	var option = document.createElement("option");
	option.text = first.options[selectedFirst].text;
	second.add(option);
	if(first.length <= selectedFirst){
		--selectedFirst;
	}
	filter();
	first.selectedIndex = selectedFirst;
	second.selectedIndex = selectedSecond;
	checkSelected();
}
function left(){
	var first = document.getElementById("first");
	var second = document.getElementById("second");
	var selectedFirst = first.selectedIndex;
	var selectedSecond = second.selectedIndex;
	if(selectedSecond == -1){
		return;
	}
	second.remove(selectedSecond);
	if(second.length <= selectedSecond){
		--selectedSecond;
	}
	filter();
	first.selectedIndex = selectedFirst;
	second.selectedIndex = selectedSecond;
	checkSelected();
}
function leftAll(){
	$("#first").empty();
	$("#second").empty();
	var first = document.getElementById("first");
	var third = document.getElementById("third").options;
	var i;
	for(i = 0; i < third.length; ++i){
		var option = document.createElement("option");
		option.text = third[i].text;
		first.add(option);
	}
	filter();
	checkSelected();
}
function sortAll(){
	sortOne();
	sortTwo();
}
function sortOne(){
	var my_options = $("#first option");

	my_options.sort(function(a,b) {
		if (a.text > b.text) return 1;
		if (a.text < b.text) return -1;
		return 0
	})

	$("#first").empty().append( my_options );
}
function sortTwo(){
	var my_options = $("#second option");

	my_options.sort(function(a,b) {
		if (a.text > b.text) return 1;
		if (a.text < b.text) return -1;
		return 0
	})

	$("#second").empty().append( my_options );
}
function checkSelected(){
	var first = document.getElementById("first");
	var second = document.getElementById("second");
	var third = document.getElementById("type");
	
	if(first.selectedIndex == -1){
		first.selectedIndex = 0;
	}
	if(second.selectedIndex == -1){
		second.selectedIndex = 0;
	}
	if(third.selectedIndex == -1){
		third.selectedIndex = 0;
	}
}
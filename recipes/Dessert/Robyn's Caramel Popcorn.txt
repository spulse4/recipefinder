Main

Ingredients
Popcorn::2 Bags
Sugar (Brown)::1 C
Syrup (Corn)::1/4 C
Margarine::1/2 C
Salt::1/2 tsp
Baking Soda::1/2 tsp

Directions
Pop popcorn and put into large paper bag.
Microwave Brown Sugar, Syrup, Butter/Margarine, and Salt till a boil.
Stir.
Microwave 2 minutes.
Add Baking Soda.
Stir.
Pour over popcorn.
Microwave 1 minute and shake.
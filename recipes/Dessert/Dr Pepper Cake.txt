Main

Ingredients
Margarine::1 C (or Butter)
Soda (Dr. Pepper)::1 C
Cocoa::4 Tbsp
Cinnamon (Ground)::1 1/2 tsp
Flour::3 C
Sugar::2 C
Salt::1/2 tsp
Egg::2 (Well Beaten)
Extract (Vanilla)::1 tsp
Buttermilk::1/2 C
Baking Soda::1 tsp
(Recipe)::recipes/Frosting/Dr Pepper Icing.txt

Directions
Preheat oven to 350 degrees F.
Heat Dr. Pepper with Margarine, but do not boil. Set aside.
Sift together Flour, Sugar, Cinnamon, Salt and Cocoa.
Beat together Eggs, Buttermilk, Vanilla and Baking Soda.
Add hot Dr. Pepper mixture to dry ingredients.
Stir in egg mixture and blend well.
Pour into greased and floured 15x10 inch sheet cake pan.
Bake for 25 minutes.
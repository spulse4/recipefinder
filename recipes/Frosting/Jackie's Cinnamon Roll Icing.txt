Extra

Ingredients
Margarine::1 Cube
Milk::5 Tbsp
Sugar (Powdered)::1 lb
Cocoa::3 1/2 Tbsp
Pecans::(Optional)::1/2 C (Chopped)

Directions
Mix all ingredients together.
Note: This makes a <b><u>LOT</u></b> of frosting, halving the recipe is a good alternative.
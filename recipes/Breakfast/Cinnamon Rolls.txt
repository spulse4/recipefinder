Main

Ingredients
Yeast::1 (.25 ounce) package (About 2 1/4 tsp)
Sugar::1/2 tsp
Water::1/2 C (Warm)
Milk::1/2C
Butter::4 Tbsp (Cut into Chuncks)
Flour::3 1/4 - 3 1/2 C (Divided)
Sugar::1/4 C
Salt::1/2 tsp
Egg::1
(Recipe)::recipes/Frosting/Jackie's Cinnamon Roll Icing.txt
Sugar (Brown)::1 C (Packed)
Cinnamon (Ground)::1 1/2 Tbsp
Butter::1/2 C (Softened)

Directions
Dough:
Mix warm water with Yeast.
Sprinkle with 1/2 tsp of Sugar.
Allow to sit for 10 minutes or until Yeast is dissolved and bubbly/foamy.
Place Milk and Butter in a microwave safe bowl. Heat on high for 1 minute 30 seconds. Butter should be at least partially melted.
Stir and set aside.
In a large mixing bowl whisk together 2 C Flour, Sugar, and Salt.
When Milk mixture has cooled to warm (not hot) add it to the Flour mixture along with the Egg while the beater (paddle attachment for those using a standard mixer) is running.
Beat until well combined, about 1 minute.
(Switch to the dough hook now) Add remaining flour only until dough barely leaves the sides of the bowl. It should be very soft and slightly sticky.
Continue to let the dough knead for 5 minutes.
If you are not using a standard mixer, turn dough out onto floured surface and knead for 5 minutes by hand.
Turn dough out onto a floured surface and let rest for about 10 minutes while you make the filling.

Filling:
Make sure Butter is softened well.
Mix with Brown Sugar and Cinnamon.

Assembly:
Prepare a clean, flat surface for rolling dough by either sprinkling with Flour or spraying with cooking spray (using cooking spray is less clean up).
Roll dough into a rectangle about 12 x 14 inches.
Spread Brown Sugar mixture (it will be slightly thick, you might have to "crumble" it) over the surface and use your fingers or the back of a spoon to gently spread around.
Roll up from the longer side of the rectangle and pinch edges closed.
Score the roll into 12 equal pieces and then cut into rolls.
Place in a 9 x 13 pan that has been sprayed with cooking spray.
Cover pan with a clean towel and let rise in a warm place for about 30 minutes - 1 hour.
In the mean time, preheat oven to 350 degrees.

Baking:
When rolls have finished rising, bake for 15-20 minutes or until light golden brown.
While the rolls are baking, prepare the icing.
Spread with icing while warm.
Makes 12 rolls.
Main

Ingredients
Milk::1 1/2 C
Sugar::1/4 C
Oil::1/4 C
Egg::2
Salt::1 tsp
Flour::6 C (About)
Yeast::3 Tbsp
Water::1/2 C
(Recipe)::recipes/Frosting/Jackie's Cinnamon Roll Icing.txt
Cinnamon (Ground)
Sugar
Pecans

Directions
Prove Yeast.
Mix yeast mixture with Sugar.
Mix remaining Ingredients.
Separate dough into four pieces (each of the four pieces makes 6 rolls).
Roll the dough into balls.
Let rolls rise until doubled in size.
Roll out the dough.
Spread Cinnamon, Sugar and Pecans on dough.
Roll it up.
Slice the rolls into appropriate sizes.
Allow cinnamon rolls to rise again.
Cook for about 20 minutes at 350 (20 max, don't overcook).
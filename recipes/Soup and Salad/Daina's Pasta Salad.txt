Main

Ingredients
Pasta::1 Box Multi Color Spiral Pasta (Cooked and Cooled)
Cucumber::1 (Cubed)
Cheese (Cheddar)::(Cubed)
Tomato (Cherry)::(Or)::Tomato (Grape)::(Whole)
Onion (Red)::1/2 (Sliced)
Olives::1 Can (Drained)
Broccoli Florets::1 C
Italian Dressing Packet Mix

Directions
Pour dressing over salad and mix just before serving.
Main

Ingredients
Zucchini::1 1/2 (Small) (Halved)
Tomato::1/4 C (Sliced)
Cheese (Cottage)::3/4 C (Low Fat)
Onion (Dried)
Cornstarch
Salt (Garlic)
Beef (Ground)::3 oz (Raw)
Cheese (Parmesan)::2 Tbsp

Directions
Remove seeds from halved Zucchini and microwave for 4 minutes.
Saute Tomato, Cottage Cheese, Dried Onion, Cornstarch, Garlic Salt, and Ground Beef.
Stuff the zucchini.
Bake at 400 degrees F for 15 minutes.
Top with Cheese.
Broil for 5 minutes until cheese is crispy.
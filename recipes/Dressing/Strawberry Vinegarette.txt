Main

Ingredients
Strawberry::1/2 lb (Fresh)
Honey::2 Tbsp (or more if your berries aren't very sweet)
Vinegar (Apple)::2 Tbsp
Oil (Olive)::2 Tbsp
Salt::1/4 tsp
Pepper::1/4 tsp (Ground)

Directions
Combine all ingredients in a blender.
Blend on high until smooth.